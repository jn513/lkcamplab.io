---
title: "About"
date: 2018-03-24T19:55:15-03:00
draft: false
---

LKCAMP is a study group for people interested in FOSS projects, with a
particular focus on the Linux Kernel. The group aims to explore, learn and teach
about the inner workings of those projects and also to help newcomers be a part
of the FOSS ecosystem.

Weekly virtual meetings are held for these activities. However since all our
members are from Brazil, Portuguese is the language used for communication.
Still, you may find [our documentation](https://docs.lkcamp.dev/) useful, which
is in English.
