---
title: "Curso de Linux - Parte I"
date: 2022-05-26
draft: false
tags: ["Curso de Linux 2022"]
categories: ["Tutorial"]
authors: ["Pedro Sader Azevedo"]
---

Como sequência ao Install Fest, o LKCAMP está conduzindo um Curso de Linux como objetivo de ensinar os novos usuários dessa família de sistemas operacionais a extrair o máximo seus computadores. O curso será dividido em três aulas: Use, Estude, e Desenvolva.

Nesta primeira aula, focaremos no aspectos práticos do uso do Linux no dia-a-dia, abordando suas funcionalidades básicas a avançadas. Os conteúdos são os seguintes:

<!-- vim-markdown-toc GFM -->

* [Conexão à eduroam](#conexão-à-eduroam)
* [Instalação de apps](#instalação-de-apps)
  * [Loja de aplicativos](#loja-de-aplicativos)
  * [Outros métodos](#outros-métodos)
    * [Repositórios PPA](#repositórios-ppa)
    * [Pacotes `.deb`](#pacotes-deb)
    * [Arquivos `.AppImage`](#arquivos-appimage)
    * [Flatpak](#flatpak)
  * [Recomendações de apps](#recomendações-de-apps)
* [Contas online](#contas-online)
* [Fluxo de trabalho](#fluxo-de-trabalho)
* [Personalização](#personalização)
  * [Aparência](#aparência)
  * [Comportamento](#comportamento)
* [Extensões](#extensões)
* [Segurança e estabilidade](#segurança-e-estabilidade)
  * [Fazer backups](#fazer-backups)
    * [Arquivos do sistema](#arquivos-do-sistema)
    * [Arquivos do usuário](#arquivos-do-usuário)
    * [Personalizações](#personalizações)
    * [Configurações](#configurações)
  * [Evitar PPAs](#evitar-ppas)
  * [Priorizar snaps](#priorizar-snaps)
  * [Cuidado com `sudo`](#cuidado-com-sudo)

<!-- vim-markdown-toc -->

Elaboramos essas aulas usando uma instalação de Ubuntu Desktop 22.04 (Jammy Jellyfish), que foi a opção de distribuição mais popular entre os participantes do nosso Install Fest.

---

# Conexão à eduroam

Antes de mais nada, vamos nos conectar a WiFi da Unicamp! Para isso, basta seguir o [tutorial oficial](https://www.ccuec.unicamp.br/ccuec/material_apoio/tutorial-eduroam-para-linux) da nossa universidade. Prometemos que essa será a única vez que você precisará usar um terminal para se conectar à internet!

---

# Instalação de apps

Agora que estamos conectados, podemos instalar programas!

## Loja de aplicativos

O principal meio de instalação de programas no Ubuntu é a loja de apps, *Ubuntu Software*. Para acessá-la, basta pesquisar por "*Ubuntu Software*" na barra de pesquisa ou clicar no seu logo da Dock.

Também é possível gerenciar seus apps, bem como suas ferramentas de desenvolvimento de software (linguagens de programação, bibliotecas, frameworks, scripts, etc), usando a linha de comando, mas isso será abordado apenas na próxima parte do curso.

{{< alerts info
"A instalação de apps pela loja é mais conveniente (instalação com um *click* e atualização automática) e mais segura (programas curados e desenvolvedores verificados) que o uso de instaladores baixados por meio de navegadores da web, como é típico no Windows."
>}}

{{< alerts success
"Por mais que usemos o termo \"loja\", todos os programas disponíveis na *Ubuntu Software* são gratuitos."
>}}

A loja de aplicativos do Ubuntu oferece programas em dois diferentes formatos empacotamento: deb e snap. O primeiro formato, mais tradicional, instala dependências de programas diretamente no seu sistema, enquanto o segundo formato, mais moderno, instala dependência de forma isolada do seu sistema.

Detalharemos as vantagens e desvantagens de cada formato na sessão de [segurança e estabilidade](#segurança-e-estabilidade), apesar da raridade com que o formato de empacotamente faz diferença na experiência do usuário.

## Outros métodos

Se você não encontrar um aplicativo na loja, é possível apelar para outros métodos de instalação de programas.

{{< alerts error
"Instalar aplicativos de fora da loja oferece riscos de estabilidade e de segurança ao seu computador."
>}}

### Repositórios PPA

É possível incluir repositórios adicionais, chamados PPAs (*Personal Package Archive*), na loja de aplicativos. Os PPAs geralmente são repositórios com um único pacote, submetidos pela comunidade ou por desenvolvedores de projetos menores a fim de facilitar a instalação de um programa no Ubuntu.

Quando adicionados à loja, os programas de PPAs são tão convenientes quanto os programas dos repositórios padrão do Ubuntu, mas são menos seguros e menso estáveis, pois não são verificados pela Canonical. Além disso, muitos PPAs são abandonados por seus criadores originais e não são atualizados para versões mais novas do sistema, o que pode comprometer processos de atualização do seu computador.

### Pacotes `.deb`

Alguns programas podem ser instalados por meio de um pacote `.deb`, baixado usando um navegador de internet. Esse método é comparável a baixar e rodar instaladores no Windows e é mais comum para programas de código fechado, que não podem ser incluídos nas lojas de apps da maioria das distribuições de GNU/Linux.

### Arquivos `.AppImage`

Outro método para rodar aplicativos que não estão na loja é usar arquivos `.AppImage`. Esse método é comparável a baixar e executar arquivos `.exe` no Windows e, assim como no Windows, as AppImages não se integram automaticamente ao seu sistema (não tem ícone no menu de apps, por exemplo) e é necessário sempre clicar no arquivo para rodar o aplicativo.

Para rodar AppImages é necessário instalar uma única dependência, usando o comando:

{{< highlight shell >}}
sudo apt install libfuse2
{{< / highlight >}}

Além disso, é necessário dar permissão de execução para o arquivo `.AppImage`. Isso pode ser feito usando o comando `chmod` ou usando o navegador de arquivos. Para isso:

1. Clique no arquivo com o botão direito e selecione `Properties`:

![](/imgs/posts/curso-de-linux-parte-i/app-image01.png)

2. Selecione `Permissions` na janela de propriedades do arquivo:

![](/imgs/posts/curso-de-linux-parte-i/app-image02.png)

3. Clique na caixa de seleção para permitir execução

![](/imgs/posts/curso-de-linux-parte-i/app-image03.png)

Feito isso, você poderá rodar a AppImage clicando no seu arquivo.

### Flatpak

Flatpak é um formato de empacotamente alternativo ao snap, desenvolvido pela RedHat que é concorrente da Canonical (sim, existe concorrência no mundo do software livre!). Por esse motivo, o Ubuntu não suporta o formato flatpak por padrão, mas ainda é possível usá-lo.

Para isso, é necessário instalar o suporte a flatpak com o comando:

{{< highlight shell >}}
sudo apt install flatpak
{{< / highlight >}}

Além disso, é preciso adicionar repositórios de apps ao flatpak. O maior e mais popular desses repositórios é o Flathub, que pode ser adicionado com o comando abaixo:

{{< highlight shell >}}
flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
{{< / highlight >}}

{{< alerts success
"Ao contrário de instaladores `.deb` e arquivos `.AppImage`, os programas obtidos no Flathub são curados e seus desenvolvedores verificados, assim como na loja de aplicativos. Por esse motivo, o flatpak não oferece riscos de segurança e estabilidade como os outros dois métodos alternativos de obtenção de programas."
>}}

Como dito anterioremente, o Ubuntu não é muito amigável ao flatpak então os aplicativos desse formato não aparecem na loja. Portanto, é necessário usar o terminal para instalar aplicativos flatpak. Os principais comandos são `flatpak search`, `flatpak install`, `flatpak remove`, e `flatpak history`.

## Recomendações de apps

- **Timeshift**: Faz snapshots do sistema (falaremos dele mais adiante!).

- **Extension Manager**: Instala, remove, e configura extensões para o ambiente gráfico do seu computador (falaremos dele mais adiante!).

- **LibreOffice**: Suíte de escritório compatível com o Microsoft Office, com programas para documentos, apresentações, planilhas, etc. Está incluso na instalação padrão do Ubuntu Desktop.

- **Bitwarden**: Gerenciador de senhas livre, aberto, e publicamente auditável. Use um gerenciador de senhas!!

- **Chromium**: Navegador de código livre e aberto em que se baseam diversos navegadores, como Chrome, Brave, Vivaldi, Opera, etc.

- **Ferdi**: Centraliza todos os seus utilitários de comunicação um um único aplicativo! Suporta WhatsApp, Telegram, Discord, Slack, Facebook Messenger, Instagram DM, Microsoft Teams, Gmail, Zoom, Meet, etc.

- **Boxes**: Programa simples e intuitivo para usar máquinas virtuais. Muito útil para testar sistemas operacionais e para criar ambientes isolados do seu sistema principal.

- **Stremio**: Uma central de mídia moderna, com acesso a muito conteúdo gratuito e capacidade de *streaming* de torrents.

---

# Contas online

Uma funcionalidade bacana do Ubuntu é a integração com contas online. Ela permite sincronizar arquivos, calendário, contatos, fotos, etc usando uma conta externa (especialmente útil para sincronizar as contas acadêmicas providenciadas pela própria Unicamp!).

Para acessar essa funcionalidade, acesse o campo "*Online Accounts*" no aplicativo de configurações e faça login na sua conta de preferência.

---

# Fluxo de trabalho

Agora que já instalamos alguns programas, vamos aprender a alavancar nosso ambiente gráfico para usá-los de forma produtiva. Para isso, é importante compreender o fluxo de trabalho (mais conhecido como "*workflow*") para o qual o ambiente gráfico do Ubuntu foi desenhado.

{{< alerts info
"No mundo Linux, a \"tecla Windows\" é chamada de \"tecla Super\"."
>}}

No Ubuntu, o recurso de produtividade mais importante é a visão de Atividades. Ela pode ser acessada pressionando a tecla Super, clicando em "Atividades" no canto superior esquerdo da tela, ou com um gesto de três dedos para cima no touchpad.

Essa tela mostra *previews* das janelas abertas nas áreas de trabalho, permitindo que você tenha uma visão geral de cada uma delas. Além disso, é possível mover as janelas para diferentes áreas de trabalho com um simples *drag and drop*, para melhor organizá-las espacialmente.

{{< alerts info
"Segurar o botão esquerdo do mouse é o mesmo que dar um duplo-click e segurar o touchpad. Isso facilita muito o *drag and drop* em laptops!"
>}}

Se você começa a digitar enquanto está na visão de Atividades, o sistema inicia uma busca global que inclui aplicativos, arquivos, configurações, eventos no calendário, caracteres (inclusive emoji!), abas de terminal, etc. Caso você selecione um aplicativo que já tem uma janela aberta, o que é indicado por uma bolinha, essa janela é focada ao invés de abrir uma nova janela (isso facilita encontrar janela que você deixou em outras área de trabalho).

O menu de aplicativos é, curiosamente, uma tela que eu não uso muito. Para acessá-lo, basta clicar no ícone mais à direita na dock ou pressionar `Super` duas vezes rápido. A maior utilidade desse menu é lançar vários aplicativos, segurando e jogando seus ícones em diferentes áreas de trabalho.

Você pode iniciar aplicativos usando a barra de pesquisa (`Ctrl + Enter` se já tiver uma janela aberta), a doca lateral de apps, ou o menu de aplicativos (`Ctrl + leftClick` ou `middleClick`, se já tiver uma janela aberta).

Ah, e claro que não é necessário usar a visão de Atividades sempre que você quiser trocar de janela ou de área de trabalho! Para isso, é possível usar os seguintes atalhos de teclado:

- Trocar de janela: `Alt + Tab`
- Trocar de aplicativo: `Super + Tab`
- Trocar para janela do mesmo aplicativo: `Super +  grave`
- Trocar de janela diretamente: `Alt + Esc`
- Ir para área de trabalho à esquerda: `Super + PageDown`
- Ir para área de trabalho à direita: `Super + PageUp`

Com esses recursos, é possível lançar aplicativos e gerenciar suas respectivas janelas de forma dinâmica, intuitiva, e produtiva!

---

# Personalização

A maioria das distribuições GNU/Linux permite que seus usuários as customizem bastante, tanto em aparência quanto em comportamento, para melhor se adaptarem às suas necessidades. O Ubuntu não é excessão!

## Aparência

Na seção "Aparência" do aplicativo de configurações é possível alterar a cor de destaque do sistema, bem como optar entre *light mode* e *dark mode*. Além disso, é possível alterar várias características da dock de aplicativos, como tamanho, posicionamento, e ocultação automática.

A combinação dessas configurações com um bom wallpaper já muda bastante a aparência do computador! Por exemplo:

| Default | Custom |
|--|--|
| ![](/imgs/posts/curso-de-linux-parte-i/default01.png) | ![](/imgs/posts/curso-de-linux-parte-i/personalizado01.png) |
| ![](/imgs/posts/curso-de-linux-parte-i/default02.png) | ![](/imgs/posts/curso-de-linux-parte-i/personalizado02.png) |

## Comportamento

Na seção "Multitarefas" do aplicativo de configurações é possível alterar diversos aspectos do comportamento do sistema. Um exemplo de configuração que eu, pessoalmente, gosto de habilitar é o "Canto ativo", que faz com que a visão de Atividades seja ativada quando o cursor do mouse passa rapidamente pelo canto superior esquero da tela.

Além disso, é possível customizar atalhos de teclado em "Veja e personalize atalhos" dentro da seção "Teclado" das configurações. Isso é útil para adicionar atalhos para programas que você usa frequentemente (eu fiz um atalho para o calendário com `Super + c`) e para mudar atalhos-padrão que você não gosta (eu substituo os atalhos de troca de área de trabalho por `Super + n`, de "*next*", e `Super + p`, de "*previous*").

---

# Extensões

Um dos recursos mais interessantes do ambiente gráfico do Ubuntu, o GNOME, são suas extensões. Assim como as extensões de navegador de internet, as extensões do GNOME adicionam novas funcionalidade de forma simples e prática!

Para usar as extensões, é necessário baixar o aplicativo "Extension Manager", disponível na loja. Depois disso, é só buscar pelas extensões que você quiser instalar usando a aba "Browse", como mostra o print abaixo.

![](/imgs/posts/curso-de-linux-parte-i/extensions.png)

Aí vai uma lista das minhas extensões favoritas:

- **GSConnect**: integra o celular ao computador, com sincronização de notificações, controle de mídia, input remoto, envio de arquivos, compartilhamento da área de transferência, e muito mais.

- **Night Theme Switcher**: alterna entre *light* e *dark mode* de acordo com o horário.

- **Alphabetical App Grid**: ordena o menu de apps alfabeticamente.

- **Tiling assistant**: adiciona layouts de janela pré-definidos e melhora a funcionalidade de divisão de tela.

- **Another Window Session Manager**: salva e restaura janelas abertas.

- **Sound Input & Output Device Chooser**: permite escolher os dispositivos de input e output sonoro diretamente no menu do sistema.

- **Application Volume Mixer**: permite escolher o volume relativo de cada janela aberta (muito útil para video-conferências!).

- **Miniview**: mostra uma miniatura de qualquer janela (muito útil para vídeo-conferências e consumo de mídia).

- **Clipboard History**: guarda o histórico da área de transferência.

- **Vitals**: adiciona um monitor do sistema ao painel.

- **ddterm**: permite acessar um terminal permanente com um atalho de teclado.

{{< alerts warning
"As extensões listadas acima são bem populares e costumam ser atualizadas para as versões mais novas do GNOME. No entanto, algumas extensões menores são abandonadas por seus desenvolvedores e não mantém compatibilidade com as últimas versões do GNOME"
>}}

# Segurança e estabilidade

Por mais que sistemas GNU/Linux sejam notórios por sua estabilidade e confiabilidade, algumas boas práticas são recomendadas para manter o seu sistema em perfeito estado. São elas:

## Fazer backups

![](/imgs/posts/curso-de-linux-parte-i/backups.png)

Backups são fundamentais em qualquer sistema operacional, para qualquer aplicação (desktop, mobile, server, etc)! Eles te protegem de corrupção de disco, perda de dados, roubo/furto, e *ransomware*.

Felizmente, as ferramentas de backup para GNU/Linux são excelentes e cobrem todos os seguintes casos de uso:

### Arquivos do sistema

Arquivos do sistema são arquivos do qual seus sistema depende para seu próprio funcionamento (ex: programas, bibliotecas, scripts de incialização, configurações de rede, etc). Vamos explicar a definição mais técnica desse conceito na próxima parte do curso!

Cópias de arquivos do sistema, ou *snapshots*, são importantes para restaurar seu computador a um estado anterior, caso seu funcionamento seja comprometido. Isso evita, por exemplo, que você precise reinstalar seu sistema operacional inteiro caso ele quebre com alguma mudança.

A melhor ferramenta para esse tipo de Backup é o Timeshift, que mencionamos na seção de [recomendações de apps](#recomendações-de-apps) já escrevemos um tutorial para ele no nosso [Guia de Instalação do Ubuntu](https://lkcamp.dev/posts/guia-de-instalacao-do-ubuntu/). 

{{< alerts error
"É possível que mudanças nos arquivos do usuário comprometam o funcionamento do seu computador (por exemplo, a instalação de uma extensão do GNOME). Nesse caso, o Timeshift não ajudaria, pois ele gerencia apenas arquivos do sistema."
>}}

Recomendamos criar um novo *snapshot* antes de fazer qualquer mudança significativa no sistema. Alguns exemplos de mudanças que me fariam tirar um *snapshot* são:

- Atualizar para uma nova versão do sistema
- Editar arquivos que precisam de permissão elevada
- Instalar pacotes `.deb` baixados da internet
- Instalar pacotes complexos com muitas dependências
- Instalar drivers adicionais
- Instalar suporte a flatpak

### Arquivos do usuário

Arquivos do usuário são seus arquivos pessoais (ex: fotos, documentos, programas que você escreveu, etc).

O Ubuntu já vem com um app de backup de arquivos pessoais instalado, cujo nome muito criativo é Backups. Esse app permite que você faça backups manuais e automáticos, para discos externos ou para serviços de armazenamento em nuvem.

### Personalizações

As personalizações que mencionamos na seção de [personalização](#personalização) são armazenadas por um programa chamado `dconf`. Esse programa permite que você guarde suas personalizações (wallpaper, cor de destaque, apps favoritos, posição da dock, etc) em um arquivo, que pode ser posteriormente usado para recuperá-las em outro computador.

Para gerar o arquivo contendo suas personalizações, use o comando:

{{< highlight shell >}}
dconf dump / > ~/.config/dconf.conf
{{< / highlight >}}

Quando quiser restaurá-las, use o comando:

{{< highlight shell >}}
dconf load / > ~/.config/dconf.conf
{{< / highlight >}}

Depois, basta incluir esse arquivo no seu backup de arquivos do usuário!

{{< alerts info
"O caminho escolhido para guardar o arquivo é sua escolha! Usamos o caminho `~/.config/dconf.conf` apenas como exemplo."
>}}

### Configurações

Fazer backups de configurações é um tópico um pouco mais avançado do que planejamos para a primeira parte do curso, pois envolve conhecimentos de linha de comando, de git, e da estrutura de arquivos do Linux. Por isso, vamos deixar aqui apenas o gostinho.

O nome do gerenciamento de arquivos de configuração é "*dotfile management*", pois arquivos os nomes de arquivos e diretórios de configuração tipicamente começa com um ponto (ex: .virmc, .bashrc, .zshrc, .config/). Caso queira começar a usar uma ferramenta para guardar esses arquivos, bem como o seu histórico de mudanças, sugerimos o `yadm`.

## Evitar PPAs

Já explicamos na seção de [instalação de apps](#repositórios-ppa) que repositórios PPA não tem o mesmo nível de confiabilidade que os repositórios-padrão do Ubuntu. Além disso, eles podem dificultar a atualização do sistema caso sejam abandonados por seus criadores originais. Por isso, é recomendado evitá-los, se possível!

## Priorizar snaps

Como dito anteriormente, snap é um formato de empacotamento moderno que isola as dependências dos aplicativos do seu sistema. Consequentemente, snaps são instalados e desinstalados de forma muito mais "limpa" e "contida", diminuindo a chance de comprometimento do seu sistema devido a erros de gerenciamento de pacotes.

É claro que essa estabilidade tem um preço: snaps ocupam mais espaço de armazenamento e demoram mais para inicializar. No entanto, eu pessoalmente acredito que essa troca vale a pena.

## Cuidado com `sudo`

O comando `sudo` dá permissões elevadas para o comando seguinte a ele, então é muito perigoso usá-lo sem entender exatamente o que está acontecendo (linhas de comando copiadas da internet, por exemplo). Quando não estiver seguro quanto a um comando com `sudo`, é uma boa fazer um *snapshot* do Timeshift antes de rodá-lo!

Isso conclui a primeira parte do Curso de Linux. Até semana que vem! :penguin:

